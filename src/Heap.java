import java.util.*;
/**
 * 
 * @author Jimmy
 *
 * @param <T>
 */
public class Heap<T extends Comparable<T>> implements PriQueueInterface<T> {
	private ArrayList<T> elements; // priority queue elements
	private int lastIndex; // index of last element in priority queue
	private int maxIndex; // index of last position in ArrayList
	/**
	 * Constructor
	 * 
	 * @param maxSize
	 */
	public Heap(int maxSize) {
		elements = new ArrayList<T>(maxSize);
		lastIndex = -1;
		maxIndex = maxSize - 1;
	}

	/**
	 * Returns true if the queue is empty, otherwise false.
	 */
	public boolean isEmpty() {
		return (lastIndex == -1);
	}
	
	/**
	 * Returns true if this queue is full; otherwise false.
	 */
	public boolean isFull() {
		return (lastIndex == maxIndex);
	}
	/**
	 * Precondition: lastIndex position is empty
	 * Inserts element into the tree and ensures shape and order
	 * 
	 * @param element
	 */
	private void reheapUp(T element) {
		int hole = lastIndex;
		while ((hole > 0)
				&& (element.compareTo(elements.get((hole - 1) / 2)) > 0)) {
			elements.set(hole, elements.get((hole - 1) / 2));
			hole = (hole - 1) / 2;
		}
		elements.set(hole, element);
	}
	/**
	 * Add element to the queue
	 */
	public void enqueue(T element) throws PriQOverflowException {
		if (lastIndex == maxIndex)
			throw new PriQOverflowException("Priority queue is full");
		else {
			lastIndex++;
			elements.add(lastIndex, element);
			reheapUp(element);
		}
	}
	/**
	 * If either child of the hole is larger then element then return the index of the larger one
	 * otherwise return index of hole
	 * @param hole
	 * @param element
	 * @return
	 */
	private int newHole(int hole, T element) {
		int left = (hole * 2) + 1;
		int right = (hole * 2) + 2;
		// hole has no children
		if (left > lastIndex)
			return hole;
		else if (left == lastIndex)	// hole has left child only
			if (element.compareTo(elements.get(left)) < 0)// element < left child
				return left;
			else// element >= left child
				return hole;
		// hole has two children
		else if (elements.get(left).compareTo(elements.get(right)) < 0)// left child < right child
				if (elements.get(right).compareTo(element) <= 0)// right child <= element
					return hole;
			else// element < right child
					return right;
		// left child >= right child
		else if (elements.get(left).compareTo(element) <= 0)// left child <= element
				return hole;
			else// element < left child
				return left;
	}
	/**
	 * Inserts element into the tree and ensures shape and order
	 * @param element
	 */
	private void reheapDown(T element) {
		int hole = 0; // current index of hole
		int newhole; // index where hole should move to

		newhole = newHole(hole, element); // find next hole
		while (newhole != hole) {
			elements.set(hole, elements.get(newhole)); // move element up
			hole = newhole; // move hole down
			newhole = newHole(hole, element); // find next hole
		}
		elements.set(hole, element); // fill in final hole
	}

	/**
	 * Removes element with highest priority from queue and returns it
	 */
	public T dequeue() throws PriQUnderflowException {
		T hold; // element to be dequeued and returned
		T toMove; // element to move down heap

		if (lastIndex == -1)
			throw new PriQUnderflowException("Priority queue is empty");
		else {
			hold = elements.get(0); // remember element to be returned
			toMove = elements.remove(lastIndex); // element to reheap down
			lastIndex--; // decrease priority queue size
			if (lastIndex != -1)
				reheapDown(toMove); // restore heap properties
			return hold; // return largest element
		}
	}
	/**
	 * toString method
	 */
	public String toString() {
		String theHeap = new String("the heap is:\n");
		for (int index = 0; index <= lastIndex; index++)
			theHeap = theHeap + index + ". " + elements.get(index) + "\n";
		return theHeap;
	}
}