/**
 * 
 * @author Jimmy
 *
 * @param <T>
 */
public class WeightedGraph<T> implements WeightedGraphInterface<T> {
	public static final int NULL_EDGE = 0;
	private static final int DEFCAP = 50;
	private int numVertices;
	private int maxVertices;
	private T[] vertices;
	private int[][] edges;
	private boolean[] marks;
	/**
	 * Default size graph
	 */
	public WeightedGraph() {
		numVertices = 0;
		maxVertices = DEFCAP;
		vertices = (T[]) new Object[DEFCAP];
		marks = new boolean[DEFCAP];
		edges = new int[DEFCAP][DEFCAP];
	}

	/**
	 * Make graph with custom size
	 * @param maxV
	 */
	public WeightedGraph(int maxV) {
		numVertices = 0;
		maxVertices = maxV;
		vertices = (T[]) new Object[maxV];
		marks = new boolean[maxV];
		edges = new int[maxV][maxV];
	}

	/**
	 * Return true if empty, otherwise false
	 */
	public boolean isEmpty(){
		return (numVertices==0);
	}
	/**
	 * Return true if graph is full, otherwise false
	 */
	public boolean isFull()	{
		return (numVertices==maxVertices);
	}
	/**
	 * Precondition: Graph not full or empty
	 * Adds vertex to graph
	 */
	public void addVertex(T vertex)	{
		vertices[numVertices] = vertex;
		for (int index = 0; index < numVertices; index++) {
			edges[numVertices][index] = NULL_EDGE;
			edges[index][numVertices] = NULL_EDGE;
		}
		numVertices++;
	}
	/**
	 * Returns true if graph has vertex, otherwise false
	 */
	public boolean hasVertex(T vertex) {
		boolean vertexFound=false;
		for (int i = 0; i < numVertices; i++) {
			if (vertices[i] == vertex)
				vertexFound = true;
		}
		return vertexFound;
	}
	/**
	 * Returns index of the vertex
	 * @param vertex
	 * @return
	 */
	private int indexIs(T vertex) {
		int index = 0;
		while (!vertex.equals(vertices[index]))
			index++;
		return index;
	}
	/**
	 * Adds edge with weight between two vertexes
	 */
	public void addEdge(T fromVertex, T toVertex, int weight) {
		int row;
		int column;

		row = indexIs(fromVertex);
		column = indexIs(toVertex);
		edges[row][column] = weight;
	}
	/**
	 * Returns weight of edge between two vertexes, if none exist then return null-edge value
	 */
	public int weightIs(T fromVertex, T toVertex) {
		int row;
		int column;

		row = indexIs(fromVertex);
		column = indexIs(toVertex);
		return edges[row][column];
	}
	/**
	 * Returns queue of adjacent vertices from selected vertex
	 */
	public UnboundedQueueInterface<T> getToVertices(T vertex) {
		UnboundedQueueInterface<T> adjVertices = new LinkedUnbndQueue<T>();
		int fromIndex;
		int toIndex;
		fromIndex = indexIs(vertex);
		for (toIndex = 0; toIndex < numVertices; toIndex++)
			if (edges[fromIndex][toIndex] != NULL_EDGE)
				adjVertices.enqueue(vertices[toIndex]);
		return adjVertices;
	}
	/**
	 * Sets all marks to false
	 */
	public void clearMarks() {
		for (int i=0;i<numVertices;i++)
			marks[i]=false;
	}
	/**
	 * Sets the mark for selected vertex to true
	 */
	public void markVertex(T vertex) {
		int i;
		i=indexIs(vertex);
		marks[i]=true;
	}
	/**
	 * Returns true if vertex is marked, otherwise false
	 */
	public boolean isMarked(T vertex) {
		int i;
		i=indexIs(vertex);
		return (marks[i]);
	}
}




































