public class GraphDemo {
	private static void shortestPaths(WeightedGraphInterface<String> graph, String startVertex)

	// Writes the shortest distance from startVertex to every
	// other reachable vertex in graph.
	{
		Flight flight;
		Flight saveFlight; // for saving on priority queue
		int minDistance;
		int newDistance;

		PriQueueInterface<Flight> pq = new Heap<Flight>(20); // Assume at most
																// 20 vertices
		String vertex;
		UnboundedQueueInterface<String> vertexQueue = new LinkedUnbndQueue<String>();

		graph.clearMarks();
		saveFlight = new Flight(startVertex, startVertex, 0);
		pq.enqueue(saveFlight);

		System.out.println("Last Vertex   Destination   Distance");
		System.out.println("------------------------------------");

		do {
			flight = pq.dequeue();
			if (!graph.isMarked(flight.getToVertex())) {
				graph.markVertex(flight.getToVertex());
				System.out.println(flight);
				flight.setFromVertex(flight.getToVertex());
				minDistance = flight.getDistance();
				vertexQueue = graph.getToVertices(flight.getFromVertex());
				while (!vertexQueue.isEmpty()) {
					vertex = vertexQueue.dequeue();
					if (!graph.isMarked(vertex)) {
						newDistance = minDistance + graph.weightIs(flight.getFromVertex(), vertex);
						saveFlight = new Flight(flight.getFromVertex(), vertex, newDistance);
						pq.enqueue(saveFlight);
					}
				}
			}
		} while (!pq.isEmpty());
		System.out.println();
	}
	
	/**
	 *Returns true if path exits on graph from startVertex to endVertex otherwise false
	 *Uses depth-first
	 * @param graph
	 * @param startVertex
	 * @param endVertex
	 * @return
	 */
	private static boolean isPath(WeightedGraphInterface<String> graph, String startVertex, String endVertex)	{
		UnboundedStackInterface<String> stack = new LinkedStack<String>();
		UnboundedQueueInterface<String> vertexQueue = new LinkedUnbndQueue<String>();
		boolean found = false;
		String vertex;
		String item;

		graph.clearMarks();
		stack.push(startVertex);
		do {
			vertex = stack.top();
			stack.pop();
			if (vertex == endVertex)
				found = true;
			else {
				if (!graph.isMarked(vertex)) {
					graph.markVertex(vertex);
					vertexQueue = graph.getToVertices(vertex);

					while (!vertexQueue.isEmpty()) {
						item = vertexQueue.dequeue();
						if (!graph.isMarked(item))
							stack.push(item);
					}
				}
			}
		} while (!stack.isEmpty() && !found);

		return found;
	}
	/**
	 * Returns true if a path exists on graph from startVertex to endVertext otherwise false
	 * Uses breadth-first search
	 * @param graph
	 * @param startVertex
	 * @param endVertex
	 * @return
	 */
	private static boolean isPath2(WeightedGraphInterface<String> graph, String startVertex, String endVertex) {
		UnboundedQueueInterface<String> queue = new LinkedUnbndQueue<String>();
		UnboundedQueueInterface<String> vertexQueue = new LinkedUnbndQueue<String>();
		boolean found = false;
		String vertex;
		String item;

		graph.clearMarks();
		queue.enqueue(startVertex);
		do {
			vertex = queue.dequeue();
			if (vertex == endVertex)
				found = true;
			else {
				if (!graph.isMarked(vertex)) {
					graph.markVertex(vertex);
					vertexQueue = graph.getToVertices(vertex);

					while (!vertexQueue.isEmpty()) {
						item = vertexQueue.dequeue();
						if (!graph.isMarked(item))
							queue.enqueue(item);
					}
				}
			}
		} while (!queue.isEmpty() && !found);

		return found;
	}

	public static void main(String[] args) {		
		WeightedGraphInterface<String> graph = new WeightedGraph<String>();		
		
		String c1 = new String("Malibu      ");
		String c2 = new String("Santa Babara");
		String c3 = new String("Barstow     ");
		String c4 = new String("Los Angeles ");
		String c5 = new String("Riverside   ");
		String c6 = new String("Palm Springs");
		String c7 = new String("San Diego   ");
		String c8 = new String("El Cajon    ");		
		String c9 = new String("Ghost Town  ");
		
		graph.addVertex(c1);
		graph.addVertex(c2);
		graph.addVertex(c3);
		graph.addVertex(c4);
		graph.addVertex(c5);
		graph.addVertex(c6);
		graph.addVertex(c7);
		graph.addVertex(c8);
		graph.addVertex(c9);

		graph.addEdge(c1,c2,45);
		graph.addEdge(c1,c4,20);
		graph.addEdge(c4,c2,30);
		graph.addEdge(c4,c1,20);
		graph.addEdge(c2,c3,45);
		graph.addEdge(c4,c5,25);
		graph.addEdge(c5,c4,25);
		graph.addEdge(c3,c5,75);
		graph.addEdge(c4,c7,100);
		graph.addEdge(c7,c5,90);
		graph.addEdge(c7,c8,15);
		graph.addEdge(c8,c5,90);
		graph.addEdge(c5,c6,75);
		graph.addEdge(c6,c3,35);
		graph.addEdge(c6,c8,120);
		graph.addEdge(c9, c7, 1000);
		
		boolean result;
		
		System.out.println("Breadth Search:");
		System.out.println();
		result = isPath2(graph, c4, c5);
		System.out.println("Los Angelos -> Riverside: " + result);
		result = isPath2(graph, c2, c5);
		System.out.println("Santa Barbara -> Riverside: " + result);
		result = isPath2(graph, c3, c5);
		System.out.println("Barstow -> Riverside: " + result);
		result = isPath2(graph, c6, c5);
		System.out.println("Palm Springs -> Riverside: " + result);
		result = isPath2(graph, c7, c5);
		System.out.println("San Diego -> Riverside: " + result);
		result = isPath2(graph, c8, c5);
		System.out.println("El Cajon -> Riverside: " + result);
		result = isPath2(graph, c8, c9);
		System.out.println("El Cajon -> Ghost Town: " + result);
		
		
		System.out.println();
		shortestPaths(graph, c1);
		System.out.println();
		shortestPaths(graph, c2);
		System.out.println();
		shortestPaths(graph, c3);
		System.out.println();
		shortestPaths(graph, c4);
		System.out.println();
		shortestPaths(graph, c5);
		System.out.println();
		shortestPaths(graph, c6);
		System.out.println();
		shortestPaths(graph, c7);
		System.out.println();
		shortestPaths(graph, c8);
		System.out.println();
		shortestPaths(graph, c9);
		
		
	}
}





















