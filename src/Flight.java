/**
 * 
 * @author Jimmy
 *
 */
public class Flight implements Comparable<Flight> {
	protected String fromVertex;
	protected String toVertex;
	protected int distance;
	/**
	 * Constructor
	 * @param fromVertex
	 * @param toVertex
	 * @param distance
	 */
	public Flight(String fromVertex, String toVertex, int distance) {
		this.fromVertex = fromVertex;
		this.toVertex = toVertex;
		this.distance = distance;
	}
	/**
	 * Return starting point
	 * @return
	 */
	public String getFromVertex() {
		return fromVertex;
	}
	/**
	 * Return end point
	 * @return
	 */
	public String getToVertex() {
		return toVertex;
	}
	/**
	 * Return distance
	 * @return
	 */
	public int getDistance() {
		return distance;
	}
	/**
	 * Set starting point
	 * @param vertex
	 */
	public void setFromVertex(String vertex) {
		fromVertex = vertex;
	}
	/**
	 * Set end point
	 * @param vertex
	 */
	public void setToVertex(String vertex) {
		toVertex = vertex;
	}
	/**
	 * Set distance
	 * @param distance
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}
	/**
	 * Compare for shorter paths
	 */
	public int compareTo(Flight other) {
		return (other.distance - this.distance); // shorter is better
	}
	/**
	 * toString method
	 */
	public String toString() {
		return (fromVertex + "    " + toVertex + "    " + distance);
	}
}