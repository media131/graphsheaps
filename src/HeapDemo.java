public class HeapDemo {
	public static void main(String[] args) {		
		PriQueueInterface<String> m = new Heap<String>(15);
		m.enqueue("M");
		m.enqueue("J");
		m.enqueue("R");
		m.enqueue("U");
		m.enqueue("W");
		m.enqueue("C");
		m.enqueue("Z");
		m.enqueue("P");
		m.enqueue("A");
		m.enqueue("F");
		System.out.println(m);
	}
}